const {fetch} = require('./helpers/fetch')
const {read} = require('./options')

// Production
const AUTH0_CLIENT_ID = 'WX1Kgy0NWL069OaiaCJLQE3OL0gqzq2e'
const AUTH0_DOMAIN = 'https://http2.au.auth0.com'
const AUTH0_DATABASE = 'Username-Password-Authentication'
const AUTH0_AUDIENCE = 'https://commons.host/'
const AUTH0_SCOPE = 'openid deploy'

async function inspectError (response) {
  let message
  try {
    const data = await response.json()
    message = data.error_description || data.description || data.error
    if (!message) throw Error
  } catch (error) {
    message = `${response.statusText} ${await response.text()}`
  }
  return new Error(message)
}

module.exports.Client = class Client {
  async options () {
    const {
      auth0: {
        client_id: clientId = AUTH0_CLIENT_ID,
        domain = AUTH0_DOMAIN,
        database = AUTH0_DATABASE,
        audience = AUTH0_AUDIENCE,
        scope = AUTH0_SCOPE
      } = {}
    } = await read()
    return {clientId, domain, database, audience, scope}
  }

  async POST (endpoint, payload) {
    const options = await this.options()
    const url = options.domain + endpoint
    const body = JSON.stringify(payload)
    const response = await fetch(url, {
      method: 'POST',
      headers: {'content-type': 'application/json'},
      body
    })
    if (response.ok) return response.json()
    else throw await inspectError(response)
  }

  async getTokenByPassword (username, password, scope) {
    const options = await this.options()
    return this.POST('/oauth/token', {
      connection: options.database,
      client_id: options.clientId,
      grant_type: 'password',
      audience: options.audience,
      scope: scope || options.scope,
      username,
      password
    })
  }

  async signup (email, password, username) {
    const options = await this.options()
    return this.POST('/dbconnections/signup', {
      client_id: options.clientId,
      connection: options.database,
      email,
      password,
      username
    })
  }
}
