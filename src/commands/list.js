const {loadingCredentials} = require('../helpers/loadingCredentials')
const {fetch} = require('../helpers/fetch')
const chalk = require('chalk')
const {domainToUnicode: toUnicode} = require('url')

module.exports.command = ['list']
module.exports.aliases = ['ls', 'll', 'la', 'l']
module.exports.desc = 'Show hosted sites'
module.exports.builder = {
}

module.exports.handler = async function handler (argv) {
  const {accessToken, origin} = await loadingCredentials()
  const url = `${origin}/v2/sites`
  const options = {
    headers: {
      authorization: `Bearer ${accessToken}`
    }
  }
  const response = await fetch(url, options)
  if (!response.ok) throw new Error(await response.text())
  const sites = await response.json()
  if (Array.isArray(sites) && sites.length > 0) {
    console.log('Sites active:')
    console.log('')
    for (const {domain, modified} of sites) {
      const location = `https://${toUnicode(domain)}`
      const timestamp = new Date(modified).toLocaleString()
      console.log(`  - ${chalk.underline.green(location)}`)
      console.log(`    ${chalk.dim('Last modified:')} ${timestamp}`)
      console.log('')
    }
  } else {
    console.log('No active websites found. Try deploying something?')
  }
}
