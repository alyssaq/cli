const {read, write} = require('../options')
const Listr = require('listr')

module.exports.command = ['logout']
module.exports.aliases = ['signout']
module.exports.desc = 'Clear the session token'
module.exports.builder = {
}

module.exports.handler = async function handler (argv) {
  await new Listr([{
    title: 'Loading options',
    task: async (context) => {
      const options = await read()
      context.options = options
    }
  }, {
    title: 'Clearing session',
    task: async ({options}) => {
      delete options.username
      delete options.access_token
      await write(options)
    }
  }]).run().catch(() => {})
}
