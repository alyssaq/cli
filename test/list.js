const test = require('blue-tape')
const {prepare} = require('./helpers')
const {run} = require('./helpers/runner')
const {createServer} = require('http')
const {resolve} = require('path')
const {readFileSync} = require('jsonfile')
const {promisify} = require('util')
const {domainToASCII: toASCII} = require('url')

test('list', async (t) => {
  const server = createServer(async (request, response) => {
    t.is(request.url, '/v2/sites')
    t.is(request.method, 'GET')
    const validToken = 'test/fixtures/valid_token.json'
    const fixture = resolve(process.cwd(), validToken)
    const {access_token: expectedBearer} = readFileSync(fixture)
    t.is(request.headers.authorization, `Bearer ${expectedBearer}`)
    response.writeHead(200)
    response.end(JSON.stringify([
      {domain: toASCII('💩.example.net'), modified: new Date().toISOString()},
      {domain: 'localhost', modified: new Date().toISOString()}
    ]))
  })
  await promisify(server.listen.bind(server))(0)

  // JWT signing key: "secret"
  await prepare('valid_token.json', {
    api: {origin: `http://localhost:${server.address().port}`}
  })

  const command = 'list'
  const {stdout, stderr} = await run(command)
  t.false(stderr)
  t.true(stdout.includes('https://💩.example.net'))
  t.true(stdout.includes('https://localhost'))

  await promisify(server.close).call(server)
})
