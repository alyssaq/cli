const {resolve} = require('path')
const {spawn, spawnSync} = require('child_process')
const read = require('read-all-stream')

const bin = resolve(process.cwd(), 'bin.js')

function runner (method, args, opts) {
  if (typeof args === 'string') {
    args = args.split(' ')
  }
  return method(process.argv[0], [bin, ...args], Object.assign({
    cwd: process.cwd(),
    timeout: 10000,
    killSignal: 'SIGKILL',
    encoding: 'utf8'
  }, opts))
}

function runSync (args, opts = {}) {
  return runner(spawnSync, args, opts)
}

function run (args, opts = {}) {
  return runner(spawn, args, opts)
}

async function runAsync (args, opts = {}) {
  const child = run(args, opts)
  child.stdout.pipe(process.stdout)
  child.stderr.pipe(process.stderr)
  const [stdout, stderr] = await Promise.all([
    read(child.stdout),
    read(child.stderr)
  ])
  return {stdout, stderr}
}

module.exports.runSync = runSync
module.exports.run = runAsync
