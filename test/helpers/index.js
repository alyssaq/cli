const {resolve} = require('path')
const {rename} = require('fs')
const {promisify} = require('util')
const userHome = require('user-home')
const readPkgUp = require('read-pkg-up')
const rimraf = require('rimraf')
const mkdirp = require('mkdirp')
const jsonfile = require('jsonfile')
const merge = require('lodash.merge')

async function mv (from, to) {
  try {
    await promisify(rename)(from, to)
  } catch (error) {
    if (error.code !== 'ENOENT') {
      throw error
    }
  }
}

async function getDirectory () {
  const {pkg} = await readPkgUp({cwd: __dirname})
  const [name] = Object.keys(pkg.bin)
  const directory = resolve(userHome, `.${name}`)
  return directory
}

let backedUp = false
async function backup () {
  const directory = await getDirectory()
  const from = directory
  const to = `${directory}.backup`
  if (backedUp === true) {
    await promisify(rimraf)(from)
  } else {
    backedUp = true
    await promisify(rimraf)(to)
    return mv(from, to)
  }
}

async function restore () {
  const directory = await getDirectory()
  const from = `${directory}.backup`
  const to = directory
  await promisify(rimraf)(to)
  try {
    return mv(from, to)
  } catch (error) {
    if (error.code !== 'ENOENT') {
      throw error
    }
  }
}

async function prepare (scenario, override) {
  await backup()
  if (scenario !== undefined) {
    const directory = await getDirectory()
    await promisify(mkdirp)(directory)
    const source = resolve(process.cwd(), 'test/fixtures', scenario)
    const destination = resolve(directory, 'options.json')
    const json = await promisify(jsonfile.readFile)(source)
    const data = merge(json, override)
    await promisify(jsonfile.writeFile)(destination, data)
  }
}

module.exports.restore = restore
module.exports.prepare = prepare
