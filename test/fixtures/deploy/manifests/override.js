module.exports = async function () {
  return [
    {
      get: '/override_manifest',
      push: '**/*'
    }
  ]
}
