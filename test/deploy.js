const test = require('blue-tape')
const {prepare} = require('./helpers')
const {run} = require('./helpers/runner')
const {createServer} = require('http')
const path = require('path')
const {promisify} = require('util')
const {backend} = require('./helpers/backend')

const fixtures = {
  directory: [
    {name: 'stuff/foo.bar', type: 'application/octet-stream'},
    {name: 'index.html', type: 'text/html'}
  ],
  configuration: {
    externalManifest: {
      manifest: [
        {
          get: '/external_manifest',
          push: '**/*'
        }
      ]
    },
    overrideManifest: {
      manifest: [
        {
          get: '/override_manifest',
          push: '**/*'
        }
      ]
    },
    simple: {foo: 'bar'}
  }
}

test('deploy', async (t) => {
  const expected = {
    domain: '💩.example.net',
    directory: fixtures.directory,
    configuration: fixtures.configuration.simple
  }
  const server = await backend(t, expected)

  // JWT signing key: "secret"
  await prepare('valid_token.json', {
    api: {origin: `http://localhost:${server.address().port}`}
  })

  const root = 'test/fixtures/deploy/root'
  const options = 'test/fixtures/deploy/options/object.js'
  const domain = '💩.example.net'
  const command = `deploy --root ${root} --options ${options} --domain ${domain} --confirm`
  const {stdout} = await run(command)
  t.ok(stdout.includes('Deployment successful'))
  t.ok(stdout.includes(`https://${expected.domain}`))

  await promisify(server.close).call(server)
})

test('deploy when options file is an async function', async (t) => {
  const server = createServer(async (request, response) => {
    request.on('end', () => {
      response.end(JSON.stringify({
        type: 'site-deploy',
        domain: 'example.com'
      }))
    })
    request.resume()
  })
  await promisify(server.listen).call(server, 0)

  // JWT signing key: "secret"
  await prepare('valid_token.json', {
    api: {origin: `http://localhost:${server.address().port}`}
  })

  const root = 'test/fixtures/deploy/root'
  const options = 'test/fixtures/deploy/options/function.js'
  const domain = 'example.net'
  const command = `deploy --root ${root} --options ${options} --domain ${domain} --confirm`
  const {stdout} = await run(command)
  t.ok(stdout.includes('Deployment successful'))
  t.ok(stdout.includes('https://example.net'))

  await promisify(server.close).call(server)
})

test('deploy fails when options file is missing', async (t) => {
  await prepare('valid_token.json', {
    api: {origin: `http://localhost:0`}
  })

  const root = 'test/fixtures/deploy/root'
  const options = 'test/fixtures/deploy/options.does_not_exist'
  const domain = 'example.net'
  const command = `deploy --root ${root} --options ${options} --domain ${domain} --confirm`
  const {stderr} = await run(command)
  t.ok(stderr.includes('Error: File not found'))
})

test('deploy fails when options file has no export', async (t) => {
  await prepare('valid_token.json', {
    api: {origin: `http://localhost:0`}
  })

  const root = 'test/fixtures/deploy/root'
  const options = 'test/fixtures/deploy/options/missing_export.js'
  const domain = 'example.net'
  const command = `deploy --root ${root} --options ${options} --domain ${domain} --confirm`
  const {stderr} = await run(command)
  t.ok(stderr.includes('TypeError: Configuration is empty'))
})

test('deploy fails when options file does not export an object', async (t) => {
  await prepare('valid_token.json', {
    api: {origin: `http://localhost:0`}
  })

  const root = 'test/fixtures/deploy/root'
  const options = 'test/fixtures/deploy/options/string.js'
  const domain = 'example.net'
  const command = `deploy --root ${root} --options ${options} --domain ${domain} --confirm`
  const {stderr} = await run(command)
  t.ok(stderr.includes('TypeError: Configuration is not an object'))
})

test('deploy inlines external manifest', async (t) => {
  const expected = {
    domain: 'example.net',
    directory: fixtures.directory,
    configuration: fixtures.configuration.externalManifest
  }
  const server = await backend(t, expected)

  await prepare('valid_token.json', {
    api: {origin: `http://localhost:${server.address().port}`}
  })

  const root = 'root'
  const options = 'options/external_manifest.js'
  const domain = 'example.net'
  const command = `deploy --root ${root} --options ${options} --domain ${domain} --confirm`
  const cwd = path.join(__dirname, 'fixtures/deploy')
  const {stdout} = await run(command, {cwd})
  t.ok(stdout.includes('Options:'))
  t.ok(stdout.includes('Manifest:'))
  t.ok(stdout.includes('Deployment successful'))
  t.ok(stdout.includes(`https://${expected.domain}`))

  await promisify(server.close).call(server)
})

test('deploy inlines default manifest', async (t) => {
  const expected = {
    domain: 'example.net',
    directory: fixtures.directory,
    configuration: fixtures.configuration.externalManifest
  }
  const server = await backend(t, expected)

  await prepare('valid_token.json', {
    api: {origin: `http://localhost:${server.address().port}`}
  })

  const root = '../root'
  const domain = 'example.net'
  const command = `deploy --root ${root} --domain ${domain} --confirm`
  const cwd = path.join(__dirname, 'fixtures/deploy/manifests')
  const {stdout} = await run(command, {cwd})
  t.not(stdout.includes('Options:'))
  t.ok(stdout.includes('Manifest:'))
  t.ok(stdout.includes('Deployment successful'))
  t.ok(stdout.includes(`https://${expected.domain}`))

  await promisify(server.close).call(server)
})

test('manifest option overrides inline manifest', async (t) => {
  const expected = {
    domain: 'example.net',
    directory: fixtures.directory,
    configuration: fixtures.configuration.overrideManifest
  }
  const server = await backend(t, expected)

  await prepare('valid_token.json', {
    api: {origin: `http://localhost:${server.address().port}`}
  })

  const root = '../root'
  const domain = 'example.net'
  const manifest = 'override.js'
  const command = `deploy --root ${root} --domain ${domain} --manifest ${manifest} --confirm`
  const cwd = path.join(__dirname, 'fixtures/deploy/manifests')
  const {stdout} = await run(command, {cwd})
  t.not(stdout.includes('Options:'))
  t.ok(stdout.includes('Manifest:'))
  t.ok(stdout.includes('Deployment successful'))
  t.ok(stdout.includes(`https://${expected.domain}`))

  await promisify(server.close).call(server)
})

test('select a specific host by domain', async (t) => {
  const domain = '💩.yep.example.net'
  const expected = {
    domain,
    directory: fixtures.directory,
    configuration: {domain}
  }
  const server = await backend(t, expected)

  await prepare('valid_token.json', {
    api: {origin: `http://localhost:${server.address().port}`}
  })

  const root = 'root'
  const options = 'options/multiple_hosts.js'
  const command = `deploy --root ${root} --options ${options} --domain ${domain} --confirm`
  const cwd = path.join(__dirname, 'fixtures/deploy')
  const {stdout} = await run(command, {cwd})
  t.ok(stdout.includes('Deployment successful'))
  t.ok(stdout.includes(`https://${expected.domain}`))

  await promisify(server.close).call(server)
})

test('avoid uploading if domain is invalid', async (t) => {
  const server = createServer(async (request, response) => {
    t.fail()
  })
  await promisify(server.listen).call(server, 0)

  await prepare('valid_token.json', {
    api: {origin: `http://localhost:${server.address().port}`}
  })

  const root = 'root'
  const domain = 'invalid_domain.example.net'
  const command = `deploy --root ${root} --domain ${domain} --confirm`
  const cwd = path.join(__dirname, 'fixtures/deploy')
  const {stdout, stderr} = await run(command, {cwd})
  t.ok(stderr.includes('Invalid domain name'))
  t.not(stdout.includes('Uploading'))

  await promisify(server.close).call(server)
})

test('accept a URL as domain', async (t) => {
  const domain = 'https://example.net:8080/something'
  const expected = {
    domain: 'example.net',
    directory: fixtures.directory,
    configuration: {}
  }
  const server = await backend(t, expected)

  await prepare('valid_token.json', {
    api: {origin: `http://localhost:${server.address().port}`}
  })

  const root = 'root'
  const command = `deploy --root ${root} --domain ${domain} --confirm`
  const cwd = path.join(__dirname, 'fixtures/deploy')
  const {stdout} = await run(command, {cwd})
  t.ok(stdout.includes('Deployment successful'))
  t.ok(stdout.includes(`https://${expected.domain}`))

  await promisify(server.close).call(server)
})
