const test = require('blue-tape')
const {prepare} = require('./helpers')
const {runSync} = require('./helpers/runner')

test('config set & get', async (t) => {
  await prepare()
  runSync('config set --foo.bar 123')
  const done = runSync('config --foo.bar')
  t.is(done.stdout, 'foo.bar 123\n')
})

test('config delete & get', async (t) => {
  await prepare('config_delete.json')
  runSync('config delete --foo.bar')
  const done = runSync('config --foo.bar')
  t.is(done.stdout, 'foo.bar undefined\n')
})

test('config show all', async (t) => {
  await prepare('config_raw.json')
  const done = runSync('config')
  t.is(done.stdout, '{ foo: \'bar\' }\n')
})

test('config reset', async (t) => {
  await prepare('config_raw.json')
  runSync('config reset')
  const done = runSync('config')
  t.is(done.stdout, '{}\n')
})
